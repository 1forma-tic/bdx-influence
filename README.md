# [Toile d'influence BDX](https://1forma-tic.frama.io/bdx-influence/)
Après chaque modificiation du projet la toile d'influence sera automatiquement actualisé en quelques minutes.

Vous pouvez [suivre l'état d'actualisation de la toile d'influence](https://framagit.org/1forma-tic/bdx-influence/-/pipelines).
- La ligne la plus haute doit avoir 2 coches vertes si l'actualisation est terminée avec succès.
- S'il y a une croix en rouge, c'est qu'il y a eu un problème. Vous pouvez cliquez sur la croix rouge pour consulter les détail et essayer de résoudre le souci. Sinon, contactez Millicent.
- Dans tout les autres cas, c'est en cours d'actualisation. Merci de bien vouloir patienter.